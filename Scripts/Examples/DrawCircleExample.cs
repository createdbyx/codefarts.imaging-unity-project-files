﻿namespace Codefarts.Imaging.Scripts
{
    using System;

    using UnityEngine;

    using Color = Codefarts.Imaging.Color;

    /// <summary>
    /// Provides an example for drawing rectangles.
    /// </summary>
    internal class DrawCircleExample : BaseExample
    {
        private bool animated;

        private bool filled;

        /// <summary>
        /// Initializes a new instance of the <see cref="DrawCircleExample"/> class.
        /// </summary>
        public DrawCircleExample()
        {
            this.image = new GenericImage<Color>(256, 256);
            this.image.Clear(Color.White);
            if (this.filled)
            {
                this.image.FillCircle(128, 128, 128, Color.Red);
            }
            else
            {
                this.image.DrawCircle(128, 128, 128, Color.Red);
            } 
            
            this.IsDirty = true;
        }

        /// <summary>
        /// Gets or sets a <see cref="GenericImage{T}"/> containing the image result for the example.
        /// </summary>
        public override GenericImage<Color> Image
        {
            get
            {
                // check if settings have changed
                if (this.changed)
                {
                    this.image.Clear(Color.White);
                    if (this.animated)
                    {
                        var value = (float)(Math.Sin(Time.time) + 1) * (this.image.Width / 2f);
                        if (this.filled)
                        {
                            this.image.FillCircle(128, 128, (int)(value / 2f), Color.Red);
                        }
                        else
                        {
                            this.image.DrawCircle(128, 128, (int)(value / 2f), Color.Red);
                        }
                    }
                    else
                    {
                        if (this.filled)
                        {
                            this.image.FillCircle(128, 128, 128, Color.Red);
                        }
                        else
                        {
                            this.image.DrawCircle(128, 128, 128, Color.Red);
                        }
                    }

                    // set changed to false
                    this.changed = false;
                }

                if (this.animated)
                {
                    this.IsDirty = true;
                }

                return this.image;
            }

            set
            {

            }
        }

        /// <summary>
        /// Gets the title of the example.
        /// </summary>
        public override string Title
        {
            get
            {
                return "Draw Circle";
            }
        }

        /// <summary>
        /// Draws the setting controls.
        /// </summary>
        public override void DrawSettings()
        {
            var value = GUILayout.Toggle(this.animated, "Animated");
            if (value != this.animated)
            {
                this.IsDirty = true;
                this.animated = value;
            }
            
             value = GUILayout.Toggle(this.filled, "Filled");
             if (value != this.filled)
            {
                this.IsDirty = true;
                this.filled = value;
            }
        }
    }
}