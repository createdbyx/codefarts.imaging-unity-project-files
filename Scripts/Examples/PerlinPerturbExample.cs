﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
namespace Codefarts.Imaging.Scripts
{
    using UnityEngine;

    using Color = Codefarts.Imaging.Color;

    /// <summary>
    /// Provides an example that uses the Perturb extension method.
    /// </summary>
    internal class PerlinPerturbExample : BaseExample
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PerlinPerturbExample"/> class.
        /// </summary>
        public PerlinPerturbExample()
        {
            this.image = new GenericImage<Color>(256, 256);
        }

        /// <summary>
        /// Gets or sets the frequency for the Perturb method.
        /// </summary>
        public float Frequency { get; set; }

        /// <summary>
        /// Gets or sets a <see cref="GenericImage{T}"/> containing the image result for the example.
        /// </summary>
        public override GenericImage<Color> Image
        {
            get
            {
                // check if settings have changed
                if (this.changed)
                {
                    // generate a perlin noise and then apply Perturb
                    var generated = new GenericImage<float>(this.image.Width, this.image.Height);
                    generated.PerlinNoise((int)this.Seed, this.Frequency);
                    generated.Perturb((int)this.Seed, this.Frequency, this.MaxHeightDistance);

                    // convert the image to a color generic image
                    generated.ForEach(
                        (bitmap, x, y) =>
                        {
                            var colorValue = (byte)(bitmap[x, y] * 255);
                            this.image[x, y] = new Color(colorValue, colorValue, colorValue, 255);
                        });

                    // set changed to false
                    this.changed = false;
                }

                return this.image;
            }

            set
            {

            }
        }

        /// <summary>
        /// Gets or sets the maximum height distance for the Perturb method.
        /// </summary>
        public float MaxHeightDistance { get; set; }

        /// <summary>
        /// Gets or sets the seed for the Perturb method.
        /// </summary>
        public float Seed { get; set; }

        /// <summary>
        /// Gets the title of the example.
        /// </summary>
        public override string Title
        {
            get
            {
                return "Perlin Noise / Perturb";
            }
        }

        /// <summary>
        /// Draws the setting controls.
        /// </summary>
        public override void DrawSettings()
        {
            GUILayout.Label("Seed: " + (int)this.Seed);
            var value = GUILayout.HorizontalSlider(this.Seed, -10f, 10f);
            if (value != this.Seed)
            {
                this.IsDirty = true;
                this.Seed = value;
            }

            GUILayout.Label("Frequency: " + this.Frequency);
            value = GUILayout.HorizontalSlider(this.Frequency, -10f, 10f);
            if (value != this.Frequency)
            {
                this.IsDirty = true;
                this.Frequency = value;
            }

            GUILayout.Label("Max Height Distance: " + this.MaxHeightDistance);
            value = GUILayout.HorizontalSlider(this.MaxHeightDistance, -100f, 100f);
            if (value != this.MaxHeightDistance)
            {
                this.IsDirty = true;
                this.MaxHeightDistance = value;
            }
        }
    }
}