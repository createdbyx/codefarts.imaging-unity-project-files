﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.Imaging
{
    public partial struct Color
    {
        public static implicit operator Color(UnityEngine.Color32 color)
        {
            return new Color(color.r, color.g, color.b, color.a);
        }

        public static implicit operator UnityEngine.Color32(Color color)
        {
            return new UnityEngine.Color32(color.R, color.G, color.B, color.A);
        }

        public static implicit operator Color(UnityEngine.Color color)
        {
            return new Color((byte)(color.r * 255), (byte)(color.g * 255), (byte)(color.b * 255), (byte)(color.a * 255));
        }

        public static implicit operator UnityEngine.Color(Color color)
        {
            return new UnityEngine.Color(color.R / 255f, color.G / 255f, color.B / 255f, color.A / 255f);
        }
    }
}
