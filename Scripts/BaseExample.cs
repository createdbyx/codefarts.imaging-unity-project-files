﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
namespace Codefarts.Imaging.Scripts
{
    /// <summary>
    /// Provides a base implementation of the <see cref="IExample"/> interface.
    /// </summary>
    public abstract class BaseExample : IExample
    {
        /// <summary>
        /// Holds a value indicating whether or not settings have changed.
        /// </summary>
        internal bool changed;

        /// <summary>
        /// Holds the value of the <see cref="IsDirty"/> property.
        /// </summary>
        private bool isDirty;

        /// <summary>
        /// Holds the value for the <see cref="Image"/> property.
        /// </summary>
        protected GenericImage<Color> image;

        /// <summary>
        /// Gets or sets a <see cref="GenericImage{T}"/> containing the image result for the example.
        /// </summary>
        public virtual GenericImage<Color> Image
        {
            get
            {
                return this.image;
            }

            set
            {
                this.image = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the example is dirty.
        /// </summary>
        public bool IsDirty
        {
            get
            {
                return this.isDirty;
            }

            set
            {
                this.isDirty = value;
                this.changed = value || this.changed;
            }
        }

        /// <summary>
        /// Gets the title of the example.
        /// </summary>
        public abstract string Title { get; }

        /// <summary>
        /// draws the setting controls.
        /// </summary>
        public virtual void DrawSettings()
        {
        }
    }
}