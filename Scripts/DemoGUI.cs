﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
namespace Codefarts.Imaging.Scripts
{
    using System.Collections.Generic;

    using UnityEngine;

    /// <summary>
    /// Provides the gui for the demo.
    /// </summary>
    [ExecuteInEditMode]
    public class DemoGUI : MonoBehaviour
    {
        /// <summary>
        /// Holds a reference to the preview texture.
        /// </summary>
        private Texture2D preview;

        /// <summary>
        /// Holds the scroll value of the example list.
        /// </summary>
        private Vector2 scroll;

        /// <summary>
        /// Holds an array of <see cref="IExample"/> implementations.
        /// </summary>
        private IExample[] examples;

        /// <summary>
        /// Holds the index for the current example.
        /// </summary>
        private int selectedIndex;

        /// <summary>
        /// Holds a reference to the current example.
        /// </summary>
        private IExample currentExample;

        /// <summary>
        /// This function is called when the object becomes enabled and active.
        /// </summary>
        public void OnEnable()
        {
            var items = new List<IExample>();
            items.Add(new DrawRectangleExample());
            items.Add(new DrawCircleExample());
            items.Add(new DrawEllipseExample());
            items.Add(new LoadRawImageExample());
            items.Add(new PerlinPerturbExample() { Seed = 0, Frequency = 6, MaxHeightDistance = 32 });
            this.selectedIndex = 0;
            this.currentExample = items[0];

            this.examples = new IExample[items.Count];     
            items.CopyTo(this.examples, 0);
        }

        /// <summary>
        /// OnGUI is called for rendering and handling GUI events.
        /// </summary>
        public void OnGUI()
        {
            GUILayout.BeginHorizontal(GUILayout.Width(Screen.width), GUILayout.Height(Screen.height));

            GUILayout.BeginVertical(GUILayout.Width(256));

            this.DrawList();
            if (this.currentExample != null)
            {
                GUILayout.BeginVertical(GUI.skin.box);
                this.currentExample.DrawSettings();
                GUILayout.EndVertical();
            }

            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();

            if (this.preview != null)
            {
                GUILayout.Label(this.preview);
            }

            GUILayout.EndHorizontal();
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        public void Update()
        {
            if (this.currentExample == null || this.currentExample.Image == null)
            {
                return;
            }

            if (this.currentExample.IsDirty)
            {
                if (this.preview == null)
                {
                    this.preview = new Texture2D(this.currentExample.Image.Width, this.currentExample.Image.Height, TextureFormat.ARGB32, false);
                }
                else if (this.preview.width != this.currentExample.Image.Width || this.preview.height != this.currentExample.Image.Height)
                {
                    GameObject.DestroyImmediate(this.preview);
                    this.preview = new Texture2D(this.currentExample.Image.Width, this.currentExample.Image.Height, TextureFormat.ARGB32, false);
                }

                this.preview.SetPixels32(this.currentExample.Image.ToUnityColor32Array());
                this.preview.Apply();

                this.currentExample.IsDirty = false;
            }
        }

        /// <summary>
        /// Draws the list of examples.
        /// </summary>
        private void DrawList()
        {
            this.scroll = GUILayout.BeginScrollView(this.scroll, false, false);

            Controls.ControlGrid.DrawGenericGrid(
                (values, index, style, options) =>
                {
                    var example = values[index];
                    var result = GUILayout.Toggle(index == this.selectedIndex, example.Title, style, options);
                    if (result && index != this.selectedIndex)
                    {
                        this.selectedIndex = index;
                        this.currentExample = example;
                        example.IsDirty = true;
                    }

                    return null;
                },
                this.examples,
                1,
                GUI.skin.button);

            GUILayout.EndScrollView();
        }
    }
}